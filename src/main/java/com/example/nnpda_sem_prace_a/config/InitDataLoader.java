package com.example.nnpda_sem_prace_a.config;

import com.example.nnpda_sem_prace_a.entity.Device;
import com.example.nnpda_sem_prace_a.entity.MyUser;
import com.example.nnpda_sem_prace_a.entity.Senzor;
import com.example.nnpda_sem_prace_a.repository.DeviceRepository;
import com.example.nnpda_sem_prace_a.repository.MyUserRepository;
import com.example.nnpda_sem_prace_a.repository.SenzorRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.UUID;

@Component
@AllArgsConstructor
public class InitDataLoader {

    private static final Random random = new Random();

    private final MyUserRepository userRepository;

    private final SenzorRepository senzorRepository;

    private final DeviceRepository deviceRepository;

    private final PasswordEncoder passwordEncoder;

    @EventListener
    public void appReady(ApplicationReadyEvent event) {
        if (!userRepository.existsByUsername("vitek") && !userRepository.existsByUsername("test")){
            MyUser user1 = createUser("vitek", "vitekrais@gmail.com", "pass");
            MyUser user2 = createUser("test", "test@test.com", "pass");
            saveDevices(user1);
            saveDevices(user2);
        }
    }

    private void saveDevices(MyUser user){
        for (int i = 0; i < random.nextInt(3)+1; i++) {
            Device device = createDevice(user);
            saveSenzors(device);
        }
    }

    private void saveSenzors(Device device){
        for (int i = 0; i < random.nextInt(3)+1; i++) {
            createSenzor(device);
        }
    }

    private MyUser createUser(String username, String email, String password) {
        MyUser user = new MyUser();
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(passwordEncoder.encode(password));
        return userRepository.save(user);
    }

    private Device createDevice(MyUser user){
        Device device = new Device();
        device.setUser(user);
        device.setLocation(UUID.randomUUID().toString());
        return deviceRepository.save(device);
    }

    private Senzor createSenzor(Device device){
        Senzor senzor = new Senzor();
        if(random.nextBoolean()){
            senzor.setType("Vlhkost");
            senzor.setValueUnit("%");
        }else {
            senzor.setType("Teplota");
            senzor.setValueUnit("°C");
        }
        senzor.setDevice(device);
        return senzorRepository.save(senzor);
    }

}
