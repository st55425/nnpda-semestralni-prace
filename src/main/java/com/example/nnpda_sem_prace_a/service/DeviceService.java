package com.example.nnpda_sem_prace_a.service;

import com.example.nnpda_sem_prace_a.dto.DeviceDto;
import com.example.nnpda_sem_prace_a.dto.DeviceInputDto;
import com.example.nnpda_sem_prace_a.entity.Device;
import com.example.nnpda_sem_prace_a.entity.MyUser;

import java.util.List;

public interface DeviceService {
    List<DeviceDto> getDevicesByUser(MyUser user);

    Device saveDevice(DeviceInputDto deviceDto);

    boolean isDeviceOwner(long deviceId, MyUser user);
}
