package com.example.nnpda_sem_prace_a.service;

import com.example.nnpda_sem_prace_a.dto.SenzorValueDto;
import com.example.nnpda_sem_prace_a.dto.SenzorValueInputDto;
import com.example.nnpda_sem_prace_a.entity.Device;
import com.example.nnpda_sem_prace_a.entity.Senzor;
import com.example.nnpda_sem_prace_a.entity.SenzorValue;
import com.example.nnpda_sem_prace_a.exception.ElementNotFoundException;
import com.example.nnpda_sem_prace_a.repository.SenzorRepository;
import com.example.nnpda_sem_prace_a.repository.SenzorValueRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional
public class SenzorValueService {

    private final SenzorValueRepository senzorValueRepository;

    private final SenzorRepository senzorRepository;

    private final DeviceService deviceService;

    private final UserService userService;

    public SenzorValue saveSenzorValue(SenzorValueInputDto valueInputDto) throws IllegalAccessException {
        Senzor senzor = senzorRepository.findById(valueInputDto.getSenzorId()).orElseThrow(ElementNotFoundException::new);
        Device device = senzor.getDevice();
        if (!deviceService.isDeviceOwner(device.getId(),  userService.getLoggedUser())){
            throw new IllegalAccessException("K danému zařízení nemáte přístup");
        }
        SenzorValue senzorValue = new SenzorValue();
        senzorValue.setSenzor(senzor);
        senzorValue.setValue(valueInputDto.getValue());
        return senzorValueRepository.save(senzorValue);
    }

    public List<SenzorValueDto> getAllValues(){
        return senzorValueRepository.findAll().stream().map(this::senzorValueToDto).collect(Collectors.toList());
    }

    public List<SenzorValueDto> getAllValuesBySenzor(long senzorId) throws IllegalAccessException {
        Senzor senzor = senzorRepository.findById(senzorId).orElseThrow(ElementNotFoundException::new);
        Device device = senzor.getDevice();
        if (!deviceService.isDeviceOwner(device.getId(),  userService.getLoggedUser())){
            throw new IllegalAccessException("K danému zařízení nemáte přístup");
        }
        return senzorValueRepository.findAllBySenzor(senzor).stream().map(this::senzorValueToDto).collect(Collectors.toList());
    }

    private SenzorValueDto senzorValueToDto(SenzorValue senzorValue){
        SenzorValueDto dto = new SenzorValueDto();
        dto.setSenzorValueId(senzorValue.getSenzorValueId());
        dto.setValue(senzorValue.getValue());
        dto.setCreatedAt(senzorValue.getCreatedAt());
        dto.setSenzorId(senzorValue.getSenzor().getSenzorId());
        return dto;
    }

}
