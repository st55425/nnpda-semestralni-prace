package com.example.nnpda_sem_prace_a.service;

import com.example.nnpda_sem_prace_a.dto.UserDto;
import com.example.nnpda_sem_prace_a.entity.MyUser;
import com.example.nnpda_sem_prace_a.entity.ResetCode;
import com.example.nnpda_sem_prace_a.repository.MyUserRepository;
import com.example.nnpda_sem_prace_a.repository.ResetCodeRespository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final PasswordEncoder passwordEncoder;

    private final MyUserRepository userRepository;

    private final ResetCodeRespository resetCodeRespository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<MyUser> myUserOptional = userRepository.getByUsername(username);
        if (myUserOptional.isPresent()) {
            MyUser myUser = myUserOptional.get();
            return new User(myUser.getUsername(), myUser.getPassword(), new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User with username: " + username + " was not found");
        }
    }

    @Override
    public void addUser(UserDto userDto) {
        MyUser user = new MyUser();
        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        if (userRepository.existsByUsername(userDto.getUsername())) {
            throw new IllegalArgumentException("Uživatel s tímto jménem již existuje");
        }
        userRepository.save(user);
    }

    @Override
    public MyUser changePassword(MyUser user, String oldPassword, String newPassword) {
        if (passwordEncoder.matches(oldPassword, user.getPassword())) {
            user.setPassword(passwordEncoder.encode(newPassword));
            return userRepository.save(user);
        } else {
            throw new IllegalArgumentException("Staré heslo nesouhlasí");
        }
    }

    @Override
    public MyUser getLoggedUser(){
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.getByUsername(username).orElseThrow();
    }

    @Override
    public MyUser newPassword(String code, String newPassword){
        ResetCode resetCode = resetCodeRespository.getByCode(code).orElseThrow(
                () -> new IllegalArgumentException("Chybný kód"));
        MyUser user = resetCode.getUser();
        user.setPassword(passwordEncoder.encode(newPassword));
        resetCodeRespository.delete(resetCode);
        return userRepository.save(user);
    }
}
