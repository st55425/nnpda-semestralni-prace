package com.example.nnpda_sem_prace_a.service;

import com.example.nnpda_sem_prace_a.entity.MyUser;
import com.example.nnpda_sem_prace_a.entity.ResetCode;
import com.example.nnpda_sem_prace_a.repository.MyUserRepository;
import com.example.nnpda_sem_prace_a.repository.ResetCodeRespository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;

    private final MyUserRepository userRepository;

    private final ResetCodeRespository resetCodeRespository;

    @Value("${spring.mail.username}")
    private String from = "";


    @Override
    public void sendResetPasswordCode(String login) {
        MyUser user = userRepository.getByUsername(login).orElseThrow(() ->
                new UsernameNotFoundException("User with username: " + login + " was not found"));


        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom(from);
        message.setTo(user.getEmail());
        message.setSubject("Kód pro obnovení hesla NNPDA");
        message.setText("Váš kód pro obnovu hesla je: " + createResetCode(user));

        javaMailSender.send(message);
    }

    private String createResetCode(MyUser user){
        String code = UUID.randomUUID().toString().substring(0, 6);
        ResetCode resetCode = new ResetCode();
        resetCode.setCode(code);
        resetCode.setUser(user);
        resetCodeRespository.save(resetCode);
        return code;
    }
}
