package com.example.nnpda_sem_prace_a.service;

public interface EmailService {
    void sendResetPasswordCode(String login);
}
