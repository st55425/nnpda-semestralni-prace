package com.example.nnpda_sem_prace_a.service;

import com.example.nnpda_sem_prace_a.dto.SenzorDto;
import com.example.nnpda_sem_prace_a.dto.SenzorInputDto;
import com.example.nnpda_sem_prace_a.entity.Senzor;

import java.util.List;

public interface SenzorService {
    List<SenzorDto> getSenzorsByDevice(long deviceId) throws IllegalAccessException;

    Senzor saveSenzor(SenzorInputDto senzorInputDto) throws IllegalAccessException;
}
