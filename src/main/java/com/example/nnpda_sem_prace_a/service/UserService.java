package com.example.nnpda_sem_prace_a.service;

import com.example.nnpda_sem_prace_a.dto.UserDto;
import com.example.nnpda_sem_prace_a.entity.MyUser;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserService extends UserDetailsService {
    @Override
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

    void addUser(UserDto userDto);

    MyUser changePassword(MyUser user, String oldPassword, String newPassword);

    MyUser getLoggedUser();

    MyUser newPassword(String code, String newPassword);
}
