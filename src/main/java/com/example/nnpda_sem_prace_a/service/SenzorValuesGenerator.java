package com.example.nnpda_sem_prace_a.service;


import com.example.nnpda_sem_prace_a.entity.Senzor;
import com.example.nnpda_sem_prace_a.entity.SenzorValue;
import com.example.nnpda_sem_prace_a.repository.SenzorRepository;
import com.example.nnpda_sem_prace_a.repository.SenzorValueRepository;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
@AllArgsConstructor
public class SenzorValuesGenerator {

    private static final Random random = new Random();

    private final SenzorRepository senzorRepository;

    private final SenzorValueRepository senzorValueRepository;

    @Scheduled(fixedDelay = 5000)
    public void generateSenzorsValues(){
        senzorRepository.findAll().forEach(this::generateSenzorValue);
    }

    private void generateSenzorValue(Senzor senzor){
        SenzorValue value = new SenzorValue();
        value.setSenzor(senzor);
        if (senzor.getType().equals("Teplota")){
            value.setValue(random.nextInt(35)-5);
        }else {
            value.setValue(random.nextInt(60)+40);
        }
        senzorValueRepository.save(value);
    }
}
