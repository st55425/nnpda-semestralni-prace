package com.example.nnpda_sem_prace_a.service;

import com.example.nnpda_sem_prace_a.dto.SenzorDto;
import com.example.nnpda_sem_prace_a.dto.SenzorInputDto;
import com.example.nnpda_sem_prace_a.entity.Device;
import com.example.nnpda_sem_prace_a.entity.Senzor;
import com.example.nnpda_sem_prace_a.exception.ElementNotFoundException;
import com.example.nnpda_sem_prace_a.repository.DeviceRepository;
import com.example.nnpda_sem_prace_a.repository.SenzorRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class SenzorServiceImpl implements SenzorService {

    private final SenzorRepository senzorRepository;

    private final DeviceRepository deviceRepository;

    private final DeviceService deviceService;

    private final UserService userService;

    @Override
    public List<SenzorDto> getSenzorsByDevice(long deviceId) throws IllegalAccessException {
        Device device = deviceRepository.findById(deviceId).orElseThrow(ElementNotFoundException::new);
        if (!deviceService.isDeviceOwner(deviceId,  userService.getLoggedUser())){
            throw new IllegalAccessException("K danému zařízení nemáte přístup");
        }
        var senzors = senzorRepository.findAllByDevice(device);
        return senzors.stream()
                .map(it -> new SenzorDto(it.getSenzorId(), it.getType())).collect(Collectors.toList());
    }

    @Override
    public Senzor saveSenzor(SenzorInputDto senzorInputDto) throws IllegalAccessException {
        Device device = deviceRepository.findById(senzorInputDto.getDeviceId()).orElseThrow(ElementNotFoundException::new);
        if (!deviceService.isDeviceOwner(device.getId(),  userService.getLoggedUser())){
            throw new IllegalAccessException("K danému zařízení nemáte přístup");
        }
        Senzor senzor = new Senzor();
        senzor.setType(senzorInputDto.getType());
        senzor.setDevice(device);
        return senzorRepository.save(senzor);
    }
}
