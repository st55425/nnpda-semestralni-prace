package com.example.nnpda_sem_prace_a.service;

import com.example.nnpda_sem_prace_a.dto.DeviceDto;
import com.example.nnpda_sem_prace_a.dto.DeviceInputDto;
import com.example.nnpda_sem_prace_a.entity.Device;
import com.example.nnpda_sem_prace_a.entity.MyUser;
import com.example.nnpda_sem_prace_a.repository.DeviceRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DeviceServiceImpl implements DeviceService {

    private final UserService userService;

    private final DeviceRepository deviceRepository;

    @Override
    public List<DeviceDto> getDevicesByUser(MyUser user){
        var devices = deviceRepository.findAllByUser(user);
        return devices.stream()
                .map(it -> new DeviceDto(it.getId(), it.getLocation())).collect(Collectors.toList());
    }

    @Override
    public Device saveDevice(DeviceInputDto deviceDto) {
        Device newDevice = new Device();
        newDevice.setLocation(deviceDto.getLocation());
        newDevice.setUser(userService.getLoggedUser());
        return deviceRepository.save(newDevice);
    }

    @Override
    public boolean isDeviceOwner(long deviceId, MyUser user){
        return deviceRepository.getById(deviceId).getUser().equals(user);
    }


}
