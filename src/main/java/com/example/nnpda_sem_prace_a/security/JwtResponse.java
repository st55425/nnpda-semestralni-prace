package com.example.nnpda_sem_prace_a.security;


import lombok.Data;

@Data
public class JwtResponse {
    private final String jwttoken;
}
