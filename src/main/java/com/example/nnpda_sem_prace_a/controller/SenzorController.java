package com.example.nnpda_sem_prace_a.controller;

import com.example.nnpda_sem_prace_a.dto.SenzorDto;
import com.example.nnpda_sem_prace_a.dto.SenzorInputDto;
import com.example.nnpda_sem_prace_a.entity.Senzor;
import com.example.nnpda_sem_prace_a.exception.ElementNotFoundException;
import com.example.nnpda_sem_prace_a.service.SenzorService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/senzor")
public class SenzorController {

    private final SenzorService senzorService;

    @GetMapping("/{deviceId}")
    public List<SenzorDto> getAlldeviceSenzors(@PathVariable long deviceId) throws IllegalAccessException {
        return senzorService.getSenzorsByDevice(deviceId);
    }

    @PostMapping()
    public Senzor saveSenzor(@RequestBody SenzorInputDto senzorInputDto) throws IllegalAccessException {
        return senzorService.saveSenzor(senzorInputDto);
    }

    @ExceptionHandler({IllegalAccessException.class})
    public ResponseEntity<?> handleException(IllegalAccessException ex) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
    }

    @ExceptionHandler({ElementNotFoundException.class})
    public ResponseEntity<?> handleException(ElementNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
    }
}
