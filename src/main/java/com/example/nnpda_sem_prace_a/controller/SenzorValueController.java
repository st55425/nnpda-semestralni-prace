package com.example.nnpda_sem_prace_a.controller;

import com.example.nnpda_sem_prace_a.dto.SenzorValueDto;
import com.example.nnpda_sem_prace_a.dto.SenzorValueInputDto;
import com.example.nnpda_sem_prace_a.entity.SenzorValue;
import com.example.nnpda_sem_prace_a.exception.ElementNotFoundException;
import com.example.nnpda_sem_prace_a.service.SenzorValueService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/senzorValue")
public class SenzorValueController {

    private final SenzorValueService senzorValueService;

    @PostMapping()
    public SenzorValue saveSenzorValue(@RequestBody SenzorValueInputDto valueInputDto) throws IllegalAccessException {
        return senzorValueService.saveSenzorValue(valueInputDto);
    }

    /*@GetMapping()
    public List<SenzorValueDto> getAllSenzorValues(){
        return senzorValueService.getAllValues();
    }*/

    @GetMapping("/{senzorId}")
    public List<SenzorValueDto> getAllSenzorValues(@PathVariable long senzorId) throws IllegalAccessException {
        return senzorValueService.getAllValuesBySenzor(senzorId);
    }

    @ExceptionHandler({IllegalAccessException.class})
    public ResponseEntity<?> handleException(IllegalAccessException ex) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
    }

    @ExceptionHandler({ElementNotFoundException.class})
    public ResponseEntity<?> handleException(ElementNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
    }
}
