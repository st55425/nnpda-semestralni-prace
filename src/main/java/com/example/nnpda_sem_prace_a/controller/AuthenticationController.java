package com.example.nnpda_sem_prace_a.controller;

import com.example.nnpda_sem_prace_a.dto.UserDto;
import com.example.nnpda_sem_prace_a.security.JwtRequest;
import com.example.nnpda_sem_prace_a.security.JwtResponse;
import com.example.nnpda_sem_prace_a.security.JwtTokenUtil;
import com.example.nnpda_sem_prace_a.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class AuthenticationController {

    private final UserService userService;

    private final AuthenticationManager authenticationManager;

    private final JwtTokenUtil jwtTokenUtil;

    @PostMapping("/login")
    public JwtResponse login(@RequestBody JwtRequest authenticationRequest) {
        return loginUser(authenticationRequest.getUsername(), authenticationRequest.getPassword());
    }

    @PostMapping("/register")
    public JwtResponse register(@RequestBody UserDto user) {
        userService.addUser(user);
        return loginUser(user.getUsername(), user.getPassword());
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<?> handleException(IllegalArgumentException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(ex.getMessage());
    }

    public JwtResponse loginUser(String username, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                username, password));
        final UserDetails userDetails = userService.loadUserByUsername(username);
        final String token = jwtTokenUtil.generateToken(userDetails);
        return new JwtResponse(token);
    }

}
