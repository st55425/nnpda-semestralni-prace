package com.example.nnpda_sem_prace_a.controller;

import com.example.nnpda_sem_prace_a.dto.DeviceDto;
import com.example.nnpda_sem_prace_a.dto.DeviceInputDto;
import com.example.nnpda_sem_prace_a.entity.Device;
import com.example.nnpda_sem_prace_a.service.DeviceService;
import com.example.nnpda_sem_prace_a.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/device")
public class DeviceController {

    private final DeviceService deviceService;

    private final UserService userService;

    @GetMapping()
    public List<DeviceDto> getAllUserDevices(){
        return deviceService.getDevicesByUser(userService.getLoggedUser());
    }

    @PostMapping()
    public Device saveDevice(@RequestBody DeviceInputDto deviceInputDto){
        return deviceService.saveDevice(deviceInputDto);
    }

}
