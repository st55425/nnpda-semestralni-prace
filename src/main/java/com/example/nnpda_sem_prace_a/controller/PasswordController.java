package com.example.nnpda_sem_prace_a.controller;

import com.example.nnpda_sem_prace_a.dto.ChangePasswordDto;
import com.example.nnpda_sem_prace_a.dto.NewPasswordDto;
import com.example.nnpda_sem_prace_a.entity.MyUser;
import com.example.nnpda_sem_prace_a.security.JwtResponse;
import com.example.nnpda_sem_prace_a.service.EmailService;
import com.example.nnpda_sem_prace_a.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/password")
@AllArgsConstructor
public class PasswordController {

    private final UserService userService;

    private final EmailService emailService;

    private final AuthenticationController authenticationController;


    @PutMapping("/change")
    public JwtResponse changePassword(@RequestBody ChangePasswordDto passwordDto) {
        MyUser user = userService.getLoggedUser();
        user = userService.changePassword(user, passwordDto.getOldPassword(), passwordDto.getNewPassword());
        return authenticationController.loginUser(user.getUsername(), passwordDto.getNewPassword());
    }

    @GetMapping("/reset/{login}")
    public void resetPassword(@PathVariable String login) {
        emailService.sendResetPasswordCode(login);
    }

    @PutMapping("/new")
    public JwtResponse newPassword(@RequestBody NewPasswordDto newPasswordDto){
        MyUser user = userService.newPassword(newPasswordDto.getCode(), newPasswordDto.getNewPassword());
        return authenticationController.loginUser(user.getUsername(), newPasswordDto.getNewPassword());
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<?> handleException(IllegalArgumentException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(ex.getMessage());
    }
}
