package com.example.nnpda_sem_prace_a.repository;

import com.example.nnpda_sem_prace_a.entity.Device;
import com.example.nnpda_sem_prace_a.entity.Senzor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SenzorRepository extends JpaRepository<Senzor, Long> {

    List<Senzor> findAllByDevice(Device device);
}
