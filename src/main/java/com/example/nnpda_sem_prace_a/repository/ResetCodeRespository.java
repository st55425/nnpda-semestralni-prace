package com.example.nnpda_sem_prace_a.repository;

import com.example.nnpda_sem_prace_a.entity.ResetCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ResetCodeRespository extends JpaRepository<ResetCode, Long> {

    Optional<ResetCode> getByCode(String code);
}
