package com.example.nnpda_sem_prace_a.repository;


import com.example.nnpda_sem_prace_a.entity.MyUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MyUserRepository extends JpaRepository<MyUser, Long> {

    Optional<MyUser> getByUsername(String username);

    boolean existsByUsername(String username);
}
