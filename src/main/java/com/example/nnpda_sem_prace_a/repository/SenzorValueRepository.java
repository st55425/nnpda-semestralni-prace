package com.example.nnpda_sem_prace_a.repository;

import com.example.nnpda_sem_prace_a.entity.Senzor;
import com.example.nnpda_sem_prace_a.entity.SenzorValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SenzorValueRepository extends JpaRepository<SenzorValue, Long> {

    List<SenzorValue> findAllBySenzor(Senzor senzor);
}
