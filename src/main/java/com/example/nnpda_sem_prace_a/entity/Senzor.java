package com.example.nnpda_sem_prace_a.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Senzor {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long senzorId;

    private String type;

    private String valueUnit;

    @ManyToOne()
    @JoinColumn(name="deviceId")
    private Device device;
}
