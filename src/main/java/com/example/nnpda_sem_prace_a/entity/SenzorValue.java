package com.example.nnpda_sem_prace_a.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class SenzorValue {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long senzorValueId;

    private long value;

    @ManyToOne()
    private Senzor senzor;

    @CreatedDate
    private LocalDateTime createdAt = LocalDateTime.now();
}
