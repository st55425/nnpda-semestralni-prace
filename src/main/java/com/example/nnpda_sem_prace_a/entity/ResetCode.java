package com.example.nnpda_sem_prace_a.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ResetCode {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long resetCodeId;

    private String code;

    @ManyToOne()
    @JoinColumn(name="userId")
    private MyUser user;
}
