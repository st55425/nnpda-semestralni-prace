package com.example.nnpda_sem_prace_a.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Device {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private String location;

    @ManyToOne()
    @JoinColumn(name="userId")
    private MyUser user;

    @OneToMany(mappedBy = "device")
    private List<Senzor> senzors;
}
