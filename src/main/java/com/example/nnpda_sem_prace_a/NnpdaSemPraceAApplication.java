package com.example.nnpda_sem_prace_a;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@EnableScheduling
public class NnpdaSemPraceAApplication {

    public static void main(String[] args) {
        SpringApplication.run(NnpdaSemPraceAApplication.class, args);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

/*
"select senzor_value.value, senzor_value.created_at, senzor.type, senzor.senzor_id, device.location from senzor_value
                      inner join senzor on senzor_value.senzor_senzor_id = senzor.senzor_id
                       inner join device on senzor.device_id = device.id
                        where log.created_at > :sql_last_value"
 */