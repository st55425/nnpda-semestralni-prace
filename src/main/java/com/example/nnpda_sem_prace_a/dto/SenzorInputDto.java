package com.example.nnpda_sem_prace_a.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SenzorInputDto {

    private String type;

    private long deviceId;
}
