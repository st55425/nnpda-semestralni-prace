package com.example.nnpda_sem_prace_a.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SenzorValueDto {

    private long senzorValueId;

    private long value;

    private long senzorId;

    private LocalDateTime createdAt;
}
